async function getUsers() {
    try {
        // fetch('https://ajax.test-danit.com/api/json/users', {                    => Я не использовал этот метод что бы была лучшая читабельность и было проще использовать
        //     method: 'GET',                                                          try catch, я  так понимаю это два одинаковых метода.
        // })
        //     .then((responseUsers) => responseUsers.json())
        //     .then((json) => console.log(json));
        //
        // fetch('https://ajax.test-danit.com/api/json/posts', {
        //     method: 'GET',
        // })
        //     .then((responsePosts) => responsePosts.json())
        //     .then((json) => console.log(json));

        const responseUsers = await fetch(
            'https://ajax.test-danit.com/api/json/users'
        );
        const responsePosts = await fetch(
            'https://ajax.test-danit.com/api/json/posts'
        );

        if (!responseUsers.ok) {
            throw new Error(`Response error:  ${responseUsers.statusText}`);
        }
        if (!responsePosts.ok) {
            throw new Error(`Response error:  ${responsePosts.statusText}`);
        }

        const users = await responseUsers.json();
        const posts = await responsePosts.json();

        console.log(users);
        console.log(posts);

        News(users, posts);
        addDeleteEventListeners();
    } catch (error) {
        alert(`Error: ${error}`);
    }
}

function firstLetter(letter) {
    return letter.charAt(0).toUpperCase() + letter.slice(1); //Для того что бы первая буква была большая
}
function News(users, posts) {
    const newsContainer = document.getElementById('newsContainer');
    const cardTemplate = document.getElementById('cardTemplate').content;

    posts.forEach((post) => {
        const { userId, title, body, image } = post;
        const user = users.find((user) => user.id === userId);

        if (user) {
            const newsClone = document.importNode(cardTemplate, true);
            const {
                avatar = 'src/images/default-avatar.png',
                name,
                email,
            } = user;

            newsClone.querySelector('.news-avatar').src = avatar;
            newsClone.querySelector('.news-username').textContent = name;
            newsClone.querySelector('.news-date').textContent =
                new Date().toLocaleString();
            newsClone.querySelector('.news-email').textContent = email;

            if (title) {
                newsClone.querySelector('.news-title').textContent =
                    firstLetter(title);
            } else {
                console.log('Title not found for post:', post);
            }

            newsClone.querySelector('.news-text').textContent =
                firstLetter(body);

            if (image) {
                newsClone.querySelector('.news-image').src = image;
            } else {
                newsClone.querySelector('.news-image').style.display = 'none';
            }

            const deleteBtn = newsClone.querySelector('.delete-button');
            deleteBtn.setAttribute('data-post-id', post.id);

            newsContainer.appendChild(newsClone);
        }
    });
}

function iconCounter() {
    setTimeout(() => {
        const icons = document.querySelectorAll('.news-footer-image');

        icons.forEach(function (icon, index) {
            const counter = icon.querySelector('.counter');
            let count = 0;

            const storedCount = localStorage.getItem(`counter_${index}`);
            if (storedCount) {
                counter.textContent = storedCount;
                counter.style.display = 'inline-block';
            }

            let clicked = localStorage.getItem(`clicked_${index}`) === 'true';

            icon.addEventListener('click', function (e) {
                e.preventDefault();

                if (!clicked) {
                    count = parseInt(counter.textContent) || 0;
                    count++;
                    counter.textContent = count;
                    counter.style.display = 'inline-block';

                    localStorage.setItem(`counter_${index}`, count);
                    localStorage.setItem(`clicked_${index}`, 'true');

                    clicked = true;
                }
            });
        });
    }, 500);
}

function addDeleteEventListeners() {
    const deleteButtons = document.querySelectorAll('.delete-button');
    deleteButtons.forEach((button) => {
        button.addEventListener('click', deleteFetch);
    });
}

async function deleteFetch(event) {
    const postId = event.target.getAttribute('data-post-id');
    try {
        const responseDelete = await fetch(
            `https://ajax.test-danit.com/api/json/posts/${postId}`,
            { method: 'DELETE' }
        );

        if (!responseDelete.ok) {
            throw new Error(`Response error: ${responseDelete.statusText}`);
        }

        const card = event.target.closest('.card');
        if (card) {
            card.remove();
        }
    } catch (error) {
        alert(`Error: ${error}`);
    }
}

window.addEventListener('load', getUsers);
window.addEventListener('DOMContentLoaded', iconCounter);
